let theme = "light";

const themeBtn = document.querySelector(".theme-btn");

themeBtn.addEventListener("click", function (e) {
    document.body.classList.toggle("dark-theme");
    if (document.body.classList.contains("dark-theme")) {
        theme = "dark";
        e.target.firstChild.classList.remove("fa-moon");
        e.target.firstChild.classList.add("fa-sun");
    } else {
        theme = "light";
        e.target.firstChild.classList.remove("fa-sun");
        e.target.firstChild.classList.add("fa-moon");
    }
    localStorage.setItem("theme", theme);
});

if (localStorage.getItem("theme") === "dark") {
    document.body.classList.add("dark-theme"); 
    themeBtn.firstChild.classList.remove("fa-moon");
    themeBtn.firstChild.classList.add("fa-sun");
}

// Якщо вибрану тему потрібно зберігати лише після перезавантаження сторінки, але повертати дефолтну після закривання вкладки
//  то можна використовувати sessionStorage замість localStorage